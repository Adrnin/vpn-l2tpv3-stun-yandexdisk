#!/bin/bash
username="yandex login"
password="yandex password"
folder="folder_vpn_data"
cid=`openssl rand -base64 10 | tr -dc _A-Z-a-z-0-9`
localport=`shuf -i 10000-65535 -n 1`
intip="10.100.100.1"
tid=`shuf -i 10-99 -n 1`
function yaread {
        curl -s --user "$1:$2" -X PROPFIND -H "Depth: 1" https://webdav.yandex.ru/$3 | sed 's/></>\n</g' | grep "displayname" | sed 's/<d:displayname>//g' | sed 's/<\/d:displayname>//g' | grep -v $3 | grep -v $4 | sort -r
}
function yacreate {
        curl -s -X MKCOL --user "$1:$2" https://webdav.yandex.ru/$3
}
function yadelete {
        curl -s -X DELETE --user "$1:$2" https://webdav.yandex.ru/$3
}
function myipport {
        stun stun.sipnet.ru -v -p $1 2>&1 | grep "MappedAddress" | sort | uniq | awk '{print $3}' | head -n1
}
function tunnel-up {
        ip l2tp add tunnel tunnel_id $7 peer_tunnel_id $7 encap udp local $3 remote $1 udp_sport $4 udp_dport $2 && ip l2tp add session name l2tp-$7 tunnel_id $7 session_id $7 peer_session_id $7
        ip link set l2tp-$7 up mtu 1488 && ip addr add $6 peer $5 dev l2tp-$7 #&& ifconfig $interface | grep " inet "
}
function tunnel-check {
        pings=0
        until [[ $pings == 1 ]]; do
                sleep 30
                if ping -c 1 $1 &>/dev/null;
                        then    echo -n .
                        else    echo -n !; pings=1
                fi
        done
}
function tunnel-down {
        ip l2tp del session tunnel_id $1 session_id $1 && ip l2tp del tunnel tunnel_id $1
}
trap 'echo -e "\nDisconnecting..." && yadelete $username $password $folder && tunnel-down $tunnelid && echo "L2PT VPN disconnected!"; exit 1' 1 2 3 8 9 14 15
until [[ -n $end ]]; do
    yacreate $username $password $folder
    until [[ -n $ip ]]; do
        mydate=`date +%s`
        timeout="60"
        list=`yaread $username $password $folder $cid | head -n1`
        yacreate $username $password $folder/$mydate:$cid
        for l in $list; do
                if [ `echo $l | sed 's/:/ /g' | awk {'print $1'}` -ge $(($mydate-65)) ]; then
                        #echo $l | sed 's/:/\n/g'
                        myipport=`myipport $localport`
                        yacreate $username $password $folder/$mydate:$cid:$myipport:$intip:$tid
                        timeout=$(( $timeout + `echo $l | sed 's/:/ /g' | awk {'print $1'}` - $mydate + 5 ))
                        ip=`echo $l | sed 's/:/ /g' | awk '{print $3}'`
                        port=`echo $l | sed 's/:/ /g' | awk '{print $4}'`
                        peer=`echo $l | sed 's/:/ /g' | awk '{print $5}'`
			peerid=`echo $l | sed 's/:/ /g' | awk '{print $6}'`
			if [[ -n $peerid ]]; then tunnelid=$(($peerid*$tid)); fi
                fi
        done
        if ( [[ -z "$ip" ]] && [ "$timeout" -gt 0 ] ) ; then
                echo -n "!"
                sleep $timeout
        fi
    done
    localip=`ip route get $ip | head -n1 | sed 's|.*src ||' | cut -d' ' -f1`
    #echo "$intip -> $localip:$localport -> $ip:$port -> $peer"
    tunnel-up $ip $port $localip $localport $peer $intip $tunnelid
    tunnel-check $peer
    tunnel-down $tunnelid
    yadelete $username $password $folder
    unset ip port myipport
    #exit 0
done
exit 0
